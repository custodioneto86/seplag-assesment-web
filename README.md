# Avaliação para desenvolvedor Fullstack Seplag (Custódio Cunha Neto)

Aqui está a implementação do frontend da avaliação para desenvolvedor.

### Prerequisitos

 - Rodar o projeto da [API](https://gitlab.com/custodioneto86/seplag-assesment-api)
 - As credeciais de acesso se encontram abaixo:

```
{
  "username": "seplag",
  "password": "123456"
}
```

