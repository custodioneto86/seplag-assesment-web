export interface SecurityUpdate {
    id: number;
    alias: string;
    documentTitle: string;
    severity: string;
    initialReleaseDate: Date;
    currentReleaseDate: Date;
    cvrfUrl: string;
}
