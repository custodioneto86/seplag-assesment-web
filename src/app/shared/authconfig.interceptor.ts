import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpRequest, HttpHandler } from "@angular/common/http";
import { RestApiService } from "./rest-api.service";
@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private restService: RestApiService) { }
    intercept(req: HttpRequest<any>, next: HttpHandler) {
        const authToken = this.restService.getToken();
        req = req.clone({
            setHeaders: {
                Authorization: "Bearer " + authToken
            }
        });
        return next.handle(req);
    }
}