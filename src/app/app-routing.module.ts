import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginErrorComponent } from './login-error/login-error.component';
import { LoginComponent } from './login/login.component';
import { SecurityUpdatesComponent } from './security-updates/security-updates.component';
import { AuthGuard } from './shared/auth.guard';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'login-error', component: LoginErrorComponent },
  { path: 'security-updates', component: SecurityUpdatesComponent, canActivate: [AuthGuard] }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule 
{ 

  
}
