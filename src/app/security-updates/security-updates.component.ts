import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { RestApiService } from '../shared/rest-api.service';
import { SecurityUpdate } from '../shared/security-update';

@Component({
  selector: 'app-security-updates',
  templateUrl: './security-updates.component.html',
  styleUrls: ['./security-updates.component.sass']
})
export class SecurityUpdatesComponent implements OnInit {

  constructor(public restApiService: RestApiService){}

  displayedColumns: string[] = ['id', 'alias', 'documentTitle', 'severity', 'initialReleaseDate', 'currentReleaseDate', 'cvrfUrl'];

  items: SecurityUpdate[] = [];
  dataSource: MatTableDataSource<SecurityUpdate> = new MatTableDataSource(this.items);

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  ngOnInit(): void {
    this.restApiService.getSecurityUpdates()  
    .subscribe(value => this.dataSource = new MatTableDataSource(value))
  }

}
