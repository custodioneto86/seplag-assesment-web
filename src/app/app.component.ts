import { Component } from '@angular/core';
import { RestApiService } from './shared/rest-api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  
  constructor(public restApiService: RestApiService) { }
  
  logout() {
    this.restApiService.doLogout()
  }
}
